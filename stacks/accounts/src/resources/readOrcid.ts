import fetch from 'node-fetch';

import { GetOrcidResponse } from '../../../../lib/interfaces/endpoints/orcid';
import { Request } from '../../../../lib/lambda/faas';

export async function readOrcid(
  context: Request<{}>,
): Promise<GetOrcidResponse> {
  if (!context.params || context.params.length !== 2) {
    throw new Error('Please specify an ORCID to fetch the public profile of.');
  }
  const orcid = context.params[1];

  const publicOrcidBasePath = ((process.env.orcid_base_path as string).indexOf('sandbox.orcid.org') !== -1)
    ?  'https://pub.sandbox.orcid.org/v2.1'
    : 'https://pub.orcid.org/v2.1';

  const fetchOptions = {
    headers: {
      Accept: 'application/json',
    },
    method: 'GET',
  };
  const fetchUrl = `${publicOrcidBasePath}/${orcid}`;

  const response = await fetch(fetchUrl, fetchOptions);
  const dataOrError = await response.json();

  if (typeof dataOrError.error !== 'undefined') {
    // Unfortunately, TypeScript isn't able to discern between union types,
    // so `dataOrError` is still of type any. See https://stackoverflow.com/q/46468882/859631
    // If you happen to want to know the structure of error messages, it's:
    // const error = dataOrError as { error: string; error_description?: string; };

    throw new Error('Could not fetch this ORCID profile, please try again.');
  }

  // Unfortunately, TypeScript isn't able to discern between union types, so
  // `dataOrError` is still of type any and can only be coerced into the correct type here.
  // See https://stackoverflow.com/q/46468882/859631
  const data = dataOrError as GetOrcidResponse;

  return data;
}
