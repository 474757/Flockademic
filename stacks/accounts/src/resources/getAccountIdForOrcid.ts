import {
  GetAccountIdResponse,
} from '../../../../lib/interfaces/endpoints/accounts';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { getAccountForOrcid } from '../dao';

export async function getAccountIdForOrcid(
  context: Request<{}> & DbContext,
): Promise<GetAccountIdResponse> {
  if (!context.params || context.params.length < 2) {
    throw(new Error('Cannot fetch account ID without an ORCID.'));
  }

  const orcid = context.params[1];

  try {
    const account = await getAccountForOrcid(context.database, orcid);

    if (account === null) {
      throw new Error('Could not find an account for this ORCID.');
    }

    return account;
  } catch (e) {
    throw new Error('Could not fetch account ID.');
  }
}
