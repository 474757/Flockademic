jest.mock('../../../../lib/validation/validators', () => ({
  isValid: jest.fn().mockReturnValue(true),
}));
jest.mock('../../../../lib/validation/periodical', () => ({
  periodicalNameValidators: [],
}));
jest.mock('../../src/services/updatePeriodical', () => ({
  updatePeriodical: jest.fn().mockReturnValue(Promise.resolve({
    name: 'Arbitrary name',
  })),
}));
jest.mock('../../src/utils/canManagePeriodical', () => ({
  canManagePeriodical: jest.fn().mockReturnValue(Promise.resolve(true)),
}));

import { updatePeriodical } from '../../src/resources/updatePeriodical';

const mockContext = {
  body: { object: { name: 'Arbitrary name' }, targetCollection: { identifier: 'Arbitrary journal slug' } },
  database: {} as any,
  session: { identifier: 'Arbitrary session ID' },

  headers: {},
  method: 'PUT' as 'PUT',
  params: [ '/00000000-0000-0000-0000-000000000000', '00000000-0000-0000-0000-000000000000', '' ],
  path: '/00000000-0000-0000-0000-000000000000',
  query: null,
};

it('should error when no properties to update were specified', () => {
  const promise = updatePeriodical({ ...mockContext, body: {} });

  return expect(promise).rejects.toEqual(new Error('Please specify properties to update.'));
});

it('should error when the user does not have a session', () => {
  const promise = updatePeriodical({
    ...mockContext,
    session: new Error('No session found'),
  });

  return expect(promise).rejects.toEqual(new Error('No session found'));
});

it('should error when no Journal ID was specified', () => {
  const promise = updatePeriodical({
    ...mockContext,
    body: {
      object: mockContext.body.object,
      targetCollection: {} as any,
    },
  });

  return expect(promise).rejects.toEqual(new Error('No journal to update specified'));
});

it('should error when the given journal is not managed by the current user', () => {
  const mockCanManagePeriodical = require.requireMock('../../src/utils/canManagePeriodical').canManagePeriodical;
  mockCanManagePeriodical.mockReturnValueOnce(false);

  const promise = updatePeriodical(mockContext);

  return expect(promise).rejects.toEqual(new Error('You can only update journals you manage.'));
});

it('should not store invalid properties', (done) => {
  const mockedService = require.requireMock('../../src/services/updatePeriodical');
  const mockedValidator = require.requireMock('../../../../lib/validation/validators');

  mockedValidator.isValid.mockReturnValueOnce(false);
  mockedValidator.isValid.mockReturnValueOnce(false);
  mockedValidator.isValid.mockReturnValueOnce(false);

  const promise = updatePeriodical(mockContext);

  setImmediate(() => {
    // The third argument is the properties to update.
    // Since they're all invalid, this should be empty.
    expect(mockedService.updatePeriodical.mock.calls[0][2]).toEqual({});

    done();
  });
});

it('should convert snake-cased date_published from the database to Schema.org\'s camelCased version', () => {
  const mockedService = require.requireMock('../../src/services/updatePeriodical');
  mockedService.updatePeriodical.mockReturnValueOnce(Promise.resolve({
    date_published: new Date('1337'),
    headline: 'Some headline',
  }));

  const promise = updatePeriodical({
    ...mockContext,
    body: {
      ...mockContext.body,
      targetCollection: {
        identifier: 'Some journal slug',
      },
    },
  });

  return expect(promise).resolves.toEqual({
    result: {
      datePublished: '1337-01-01T00:00:00.000Z',
      headline: 'Some headline',
    },
    targetCollection: {
      identifier: 'Some journal slug',
    },
  });
});

it('should error when the service errors', () => {
  const mockedService = require.requireMock('../../src/services/updatePeriodical');
  mockedService.updatePeriodical.mockReturnValueOnce(Promise.reject(new Error('Some error')));

  const promise = updatePeriodical(mockContext);

  return expect(promise).rejects.toEqual(new Error('There was a problem modifying the journal, please try again.'));
});
