---------------------------------------------------------
--                      WARNING                        --
-- Do not modify this script after merging it to       --
-- `dev`. Only add new, consecutively numbered         --
-- changesets in case the schema needs further         --
-- changes.                                            --
---------------------------------------------------------

-- NOTE: When running locally. Docker Compose won't pick up changes in SQL files unless you recreate the volume:
--       docker-compose rm -v; docker-compose up --build;

BEGIN;
  CREATE TABLE periodicals (
    identifier VARCHAR(36) PRIMARY KEY,
    uuid UUID UNIQUE NOT NULL,
    name TEXT,
    headline TEXT,
    description TEXT,
    date_published TIMESTAMP,
    creator_session UUID NOT NULL,
    date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
  );
  CREATE INDEX periodicals_uuid_index ON periodicals(uuid);
  CREATE INDEX periodicals_creator_session_index ON periodicals(creator_session);

  CREATE TABLE periodical_creators (
    periodical UUID NOT NULL REFERENCES periodicals(uuid),
    creator UUID NOT NULL,
    PRIMARY KEY (periodical, creator)
  );

  CREATE TABLE scholarly_articles (
    identifier UUID PRIMARY KEY,
    name TEXT,
    description TEXT,
    creator_session UUID NOT NULL
  );

  CREATE TABLE scholarly_articles_part_of (
    scholarly_article UUID NOT NULL REFERENCES scholarly_articles(identifier),
    is_part_of UUID NOT NULL REFERENCES periodicals(uuid),
    date_published TIMESTAMP,
    PRIMARY KEY (scholarly_article, is_part_of)
  );

  CREATE TABLE scholarly_article_associated_media (
    scholarly_article UUID NOT NULL REFERENCES scholarly_articles(identifier),
    identifier BIGSERIAL PRIMARY KEY,
    content_url text NOT NULL,
    name text NOT NULL,
    date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
  );
  CREATE INDEX scholarly_article_associated_media_index ON scholarly_article_associated_media(identifier);

  CREATE TABLE scholarly_article_authors (
    scholarly_article UUID NOT NULL REFERENCES scholarly_articles(identifier),
    author UUID NOT NULL,
    date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (scholarly_article, author)
  );
COMMIT;
