import { getMaxLengthValidator, getMinLengthValidator } from './validators';

export const periodicalDescriptionValidators = [];
export const periodicalHeadlineValidators = [];

export const periodicalNameValidators = [
  getMinLengthValidator<string>({ minLength: 1 }),
];

export const periodicalSlugValidators = [
  getMinLengthValidator<string>({ minLength: 1 }),
  // UUID's are 36 characters long, so this helps us disambiguate:
  getMaxLengthValidator<string>({ maxLength: 35 }),
];
